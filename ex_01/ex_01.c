int array_sum(int *tab, int size)
{
  int i = -1;
  int sum;

  sum = 0;
  while (++i < size)
    sum += tab[i];
  return sum;
}
