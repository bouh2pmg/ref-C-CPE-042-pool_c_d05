#include <string.h>
#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

static int my_putnbr_(int nb, int b, char *base)
{
  int tmp;

  tmp = nb;
  if (nb > b || nb < -b)
    nb = nb - my_putnbr_(nb / b, b, base) * b;
  if (nb < 0)
    {
      nb *= -1;
      if (tmp > -b)
	my_putchar('-');
    }
  my_putchar(base[nb]);
  return tmp;
}

int my_putnbr_base(int nbr, char *base)
{
  int b = strlen(base);
  int i = 0;
  
  if (1 == b)
    {
      while (i++ < nbr)
	my_putchar(base[0]);
      return nbr;
    }
  return my_putnbr_(nbr, b, base);
}
