#include <stdlib.h>
#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

void my_putnbr(int nb)
{
  if (nb == -2147483648)
    {
      write(1, "-2147483648", 11);
      return ;
    }

  if (nb < 0)
    {
      write(1, "-", 1);
      nb *= -1;
    }

  if (nb > 9)
    my_putnbr(nb / 10);
  my_putchar((nb % 10) + '0');
}
